<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigFileController extends Controller
{
    public function index(Request $request)
    {
          if($request->ajax())
          {
            $TEDDConfigFile = $_GET['data'];
            //$msg = 'getRequest has loaded completly! ' . $dataloaded
            //return $msg;
            //$filename = "tedd_project";
            $path = storage_path() . "/tedd/projectConfigFiles/" . $TEDDConfigFile;


            $json = json_decode(file_get_contents($path), true);
            $json_string = json_encode($json, JSON_PRETTY_PRINT);

            return $json_string;
          }
    }

    public function openTestCase(Request $request)
    {
          if($request->ajax())
          {
            $TEDDTestCaseFile = $_GET['data'] . ".xml";
            $path = "tedd/testCaseFiles/" . $TEDDTestCaseFile;

            $contents = \File::get(storage_path($path));

            return $contents;
            //return "Ajax is performed sucessfully! Opening file: " . $TEDDTestCaseFile . ' on path: ' . $path;
          }
    }

    public function saveTestCase(Request $request)
    {
          if($request->ajax())
          {
            $TEDDTestCaseFile = $_GET['data'] . ".xml";
            $path = "tedd/testCaseFiles/" . $TEDDTestCaseFile;

            $xml = $_GET['xml'];

            \File::put(storage_path($path), $xml);

            //$contents = \File::get(storage_path($path));

            return 'Ajax is performed sucessfully! Path: ' . $path . ' XML: ' . $xml;
            //return "Ajax is performed sucessfully! Opening file: " . $TEDDTestCaseFile . ' on path: ' . $path;
          }
    }
}
