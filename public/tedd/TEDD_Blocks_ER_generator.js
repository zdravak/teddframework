Blockly.PHP['tedd_tcttemplate_blank'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = 'Expected results dummy text for TCT_Template block\n';

  return code;
};

Blockly.PHP['import'] = function(block) {
  var dropdown_import_value = block.getFieldValue('import_value');
  // TODO: Assemble Python into code variable.  
  
  var code = "";
  //var code = 'Expected results dummy text for import block\n';  
  
  return code;
  
};

Blockly.PHP['import_custom'] = function(block) {
  var text_import_value = block.getFieldValue('import_value');
  // TODO: Assemble Python into code variable.
  var code = "";
  //var code = 'Expected results dummy text for import_custom block\n'; 
  
  return code;
};


//KLEMA 15 s if-om i logovima
Blockly.PHP['klema_15'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  gStepER++;
  
  var code = gStepER + '. KL15 performed sucessfully.\n';
  
  //var code = 'Expected results dummy text for import_custom block'; 
  
  return code;
};

Blockly.PHP['start_board'] = function(block) {
  var value_state = Blockly.PHP.valueToCode(block, 'state', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);

  gStepER++;
  var code = gStepER + '. zFAS is in state ' + value_state + '.\n';

  return code;
};

Blockly.PHP['check_dtc_status'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_dtc = Blockly.PHP.valueToCode(block, 'dtc', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  
  gStepER++;
  var code = gStepER + '. DTC ' + value_dtc + ' is active.\n';
  
  return code;
};

Blockly.PHP['add_condition'] = function(block) {
  var value_host = Blockly.PHP.valueToCode(block, 'host', Blockly.PHP.ORDER_ATOMIC);
  var value_condition = Blockly.PHP.valueToCode(block, 'condition', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  
  var code = '';
  
  return code;
};

Blockly.PHP['check_condition'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_host = Blockly.PHP.valueToCode(block, 'host', Blockly.PHP.ORDER_ATOMIC);
  var value_condition = Blockly.PHP.valueToCode(block, 'condition', Blockly.PHP.ORDER_ATOMIC);
  var value_timeout = Blockly.PHP.valueToCode(block, 'timeout', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  
  gStepER++;
  
  var code_ = checkbox_positive_check ? 'contains' : 'does not contain';
  
  //var code = 'STEP ' + gStepPTC + ': Expected results dummy text for check_condition block\n';
  var code = gStepER + '. String ' + value_condition + ' is present on ' + value_host + ' UART.\n';
  
  return code;
};

Blockly.PHP['klema_30'] = function(block) {
  var value_step = Blockly.PHP.valueToCode(block, 'step', Blockly.PHP.ORDER_ATOMIC);

  gStepER++;
  
  var code = gStepER + '. KL30 performed sucessfully.\n';
  
  return code;
};

//Stanje ploce s if-ovima i logovima
Blockly.PHP['check_board_status'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_state = Blockly.PHP.valueToCode(block, 'state', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  
  gStepER++;
  var code = gStepER + '. zFAS is in state ' + value_state + '.\n';
  
  return code;
};





//MUTATORS - TEST
Blockly.PHP['tedd_header'] = function(block) {
  // Create a list with any number of elements of any type.
  var elements = new Array(block.itemCount_);
  for (var i = 0; i < block.itemCount_; i++) {
    elements[i] = Blockly.PHP.valueToCode(block, 'ADD' + i,
        Blockly.PHP.ORDER_NONE) || 'None';
  }
  
  var code = "";
  //var code = 'Expected results dummy text for tedd_header block\n';
  
  /*var code = '##############\n\
# TC Header\n\
##############\n\
tc_header = {\n' + elements.join(',\n') + '}';
  return [code, Blockly.PHP.ORDER_ATOMIC];*/
  
  return code;
};

Blockly.PHP['tedd_header_new_item'] = function(block) {
  var value_entry_name = Blockly.PHP.valueToCode(block, 'entry_name', Blockly.PHP.ORDER_ATOMIC);
  var value_entry_value = Blockly.PHP.valueToCode(block, 'entry_value', Blockly.PHP.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  /*var code = value_entry_name + '\t:\t' + value_entry_value;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];*/
  
  var code = '';
  
  return code;
};


Blockly.PHP['flashdataset'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_ds_name = Blockly.PHP.valueToCode(block, 'DS_name', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  
  var code = 'Expected results dummy text for flashdataset block\n';

  return code;
};

Blockly.PHP['checkerrorehm'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_error_id = Blockly.PHP.valueToCode(block, 'error_id', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  
  gStepER++;
  
  var code_ = checkbox_positive_check ? 'is present' : 'is not present';
  
  var code = gStepER + '. errorID = ' + value_error_id + ' ' + code_ + ' in EHM.\n';
  
  return code;
};

Blockly.PHP['triggererror'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_host = Blockly.PHP.valueToCode(block, 'host', Blockly.PHP.ORDER_ATOMIC);
  var value_error_id = Blockly.PHP.valueToCode(block, 'error_id', Blockly.PHP.ORDER_ATOMIC);
  var value_errorid = Blockly.PHP.valueToCode(block, 'errorID', Blockly.PHP.ORDER_ATOMIC);
  
  gStepER++;
  
  var code = gStepER + '. Interface for errorID = ' + value_error_id + ' sucessfully triggered.\n';

  return code;
};