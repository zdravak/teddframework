 //alert('PTC Generator started!');

Blockly.Lua['tedd_tcttemplate_blank'] = function(block) {
  // TODO: Assemble Python into code variable.
  //var code = 'PTC generator dummy text for TCT_Template block\n';
  gStepPTC = gStepPTC + 1;

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. Execution code for ' + block.type + '\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  return code;
};

Blockly.Lua['import'] = function(block) {
  var dropdown_import_value = block.getFieldValue('import_value');
  // TODO: Assemble Python into code variable.

  var code = "";

  return code;

};

Blockly.Lua['import_custom'] = function(block) {
  var text_import_value = block.getFieldValue('import_value');
  // TODO: Assemble Python into code variable.

  var code = "";
  //var code = 'PTC generator dummy text for import_custom block\n';

  return code;
};


//KLEMA 15 s if-om i logovima
Blockly.Lua['klema_15'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  gStepPTC = gStepPTC + 1;

  AddImportBlock("Utils.utilities");
  //PreconditionsCheckBoxVisibility(block);

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. Perform KL15.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  //var code = gStepPTC + '. Perform KL15.\n';

  //var code = 'PTC generator dummy text for import_custom block';

  return code;
};

Blockly.Lua['start_board'] = function(block) {
  var value_state = Blockly.Lua.valueToCode(block, 'state', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);

  gStepPTC++;

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. zFAS is in state ' + value_state + '.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  //var code = gStepPTC + '. zFAS is in state ' + value_state + '.\n';

  return code;
};

Blockly.Lua['check_dtc_status'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_dtc = Blockly.Lua.valueToCode(block, 'dtc', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.

  AddImportBlock("Utils.API_Diagnostic");

  gStepPTC++;

  var preconditionCode = '• Precondition code for ' + block.type;
  var executionCode = gStepPTC + '. Verify that DTC ' + value_dtc + ' is active.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  /*
  var rootBlock = block.getRootBlock();
  var rootName = "";
  if(rootBlock)
	rootName = rootBlock.getFieldValue("NAME");

  var checkbox_field = block.getField('positive_check');

  if(rootName == "Preconditions")
  {
	//alert("Checkbox disabled");
	checkbox_field.setVisible(false);
  }
  else
  {
	//alert("Checkbox enabled");
	checkbox_field.setVisible(true);
  }*/

  //PreconditionsCheckBoxVisibility(block);

  return code;
};

Blockly.Lua['add_condition'] = function(block) {
  var value_host = Blockly.Lua.valueToCode(block, 'host', Blockly.Lua.ORDER_ATOMIC);
  var value_condition = Blockly.Lua.valueToCode(block, 'condition', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.

  var code = '';

  return code;
};

Blockly.Lua['check_condition'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_host = Blockly.Lua.valueToCode(block, 'host', Blockly.Lua.ORDER_ATOMIC);
  var value_condition = Blockly.Lua.valueToCode(block, 'condition', Blockly.Lua.ORDER_ATOMIC);
  var value_timeout = Blockly.Lua.valueToCode(block, 'timeout', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);

  gStepPTC = gStepPTC + 1;

  var code_ = checkbox_positive_check ? 'contains' : 'does not contain';

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. Check if ' + value_host + ' UART ' +  code_ + ' ' + value_condition + '.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  //var code = 'STEP ' + gStepPTC + ': PTC generator dummy text for check_condition block\n';
  //var code = gStepPTC + '. Check if ' + value_host + ' UART ' +  code_ + ' ' + value_condition + '.\n';
  return code;
};

Blockly.Lua['klema_30'] = function(block) {
  var value_step = Blockly.Lua.valueToCode(block, 'step', Blockly.Lua.ORDER_ATOMIC);

  gStepPTC = gStepPTC + 1;

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. Perform KL30.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  //var code = gStepPTC + '. Perform KL30.\n';

  return code;
};

//Stanje ploce s if-ovima i logovima
Blockly.Lua['check_board_status'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_state = Blockly.Lua.valueToCode(block, 'state', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);

  gStepPTC = gStepPTC + 1;

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. Wait until zFAS is in state ' + value_state + '.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  //var code = gStepPTC + '. Wait until zFAS is in state ' + value_state + '.\n';

  return code;
};





//MUTATORS - TEST
Blockly.Lua['tedd_header'] = function(block) {
  // Create a list with any number of elements of any type.
  var elements = new Array(block.itemCount_);
  for (var i = 0; i < block.itemCount_; i++) {
    elements[i] = Blockly.Lua.valueToCode(block, 'ADD' + i,
        Blockly.Lua.ORDER_NONE) || 'None';
  }

  var code = "";
  //var code = 'PTC generator dummy text for tedd_header block\n';

  /*var code = '##############\n\
# TC Header\n\
##############\n\
tc_header = {\n' + elements.join(',\n') + '}';
  return [code, Blockly.Lua.ORDER_ATOMIC];*/

  return code;
};

Blockly.Lua['tedd_header_new_item'] = function(block) {
  var value_entry_name = Blockly.Lua.valueToCode(block, 'entry_name', Blockly.Lua.ORDER_ATOMIC);
  var value_entry_value = Blockly.Lua.valueToCode(block, 'entry_value', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  /*var code = value_entry_name + '\t:\t' + value_entry_value;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];*/

  if(value_entry_name.toUpperCase() == "'BUILD'")
  {
	  gBuild = value_entry_value;
  }
  else if(value_entry_name.toUpperCase() == "'DATA SET'")
  {
	  gData_set = value_entry_value;
  }
  else if(value_entry_name.toUpperCase() == "'PTC_ID'")
  {
	  gPTCID = value_entry_value;
  }

  var code = '';

  return code;
};


Blockly.Lua['flashdataset'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_ds_name = Blockly.Lua.valueToCode(block, 'DS_name', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);

  //var code = "TEST";


  var rootBlock = block.getRootBlock();
  var rootName = "";
  if(rootBlock)
	rootName = rootBlock.getFieldValue("NAME");

  var code;

  if(rootName == "Preconditions")
  {
	  gData_set = value_ds_name;
	  code = '';
  }
  else
  {
	  code = 'PTC generator dummy text for flashdataset block\n';
  }

  //PreconditionsCheckBoxVisibility(block);

  return code;
};

Blockly.Lua['checkerrorehm'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_error_id = Blockly.Lua.valueToCode(block, 'error_id', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);

  gStepPTC++;
  var code_ = checkbox_positive_check ? 'is present' : 'is not present';

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. Execute Read Data By Identifier / Measurement Value / Platform_zFAS_Error_Handler_Master and check that errorID = ' + value_error_id + ' ' + code_ + '.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  //var code = gStepPTC + '. Execute Read Data By Identifier / Measurement Value / Platform_zFAS_Error_Handler_Master and check that errorID = ' + value_error_id + ' ' + code_ + '.\n';

  return code;
};

Blockly.Lua['triggererror'] = function(block) {
  var checkbox_positive_check = block.getFieldValue('positive_check') == 'TRUE';
  var value_host = Blockly.Lua.valueToCode(block, 'host', Blockly.Lua.ORDER_ATOMIC);
  var value_error_id = Blockly.Lua.valueToCode(block, 'error_id', Blockly.Lua.ORDER_ATOMIC);
  var value_errorid = Blockly.Lua.valueToCode(block, 'errorID', Blockly.Lua.ORDER_ATOMIC);

  gStepPTC++;

  //var code = gStepPTC + '. VIA RTE trigger errorID = 80.\n';

  var preconditionCode = '• Precondition code for ' + block.type + '\n';
  var executionCode = gStepPTC + '. VIA RTE trigger errorID = 80.\n';

  var code = CreateCode(block, preconditionCode, executionCode);

  AddImportBlock("Utils.API_RTE as RTE");

  return code;
};
