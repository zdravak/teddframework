# README #

To test TEDD open following HTML:

tedd/google-blockly-e0be7e4/demos/code/index_RTRK_test.html

To prune Blockly:
1. Remove all top folders except google-blockly and TEDD
2. Move tedd folder one level up (to google-blockly)
3. Remove folders from google-blockly: core_backup, demos, i18n, tests
4. In tedd files index.html and core.js replace string "../../" with "../"
5. Move tedd files index.html, tedd.css and scripts folder to google-blockly directory
6. Move tedd/msg/en.js file to google-blockly/msg
7. Move all from tedd/media dir to google-blockly/media
8. Update all paths in index.html and core.js (../ -> none)
9. Move top folder TEDD to google-blockly
10. Update paths in index.html refering to the files in TEDD folder.