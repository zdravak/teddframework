'use strict';

/**
 * Create a namespace for the application.
 */
var Code = {};
//var tmpCode;
//var gStep = 0;

  var gProjectConfigFile;

  var gStepPTC;
  var gStep;

  var gMaxSearchResults = 20;
  var gNumberOfDisplayedBlocks = 10;

  var gStepER;

  var gUpdateHeaderBlock = 0;
  var gProjectName;
  var gProjectDescription;
  var gBuild = '';
  var gData_set = '';
  var gHeaderItemsNumber = 3;
  var gXMLHeaderConfigItems = "";

  var gAddImportY = 50;

  var gImportCode = "";
  var gHeaderCode = "";

  var gPTCID;
  var gMeasurementResult = true;
  var gMeasurementErrID = 0;

  //PHP is ER generator
  //Lua is PTC generator

/**
 * Lookup for names of supported languages.  Keys should be in ISO 639 format.
 */
Code.LANGUAGE_NAME = {
  'ar': 'العربية',
  'be-tarask': 'Taraškievica',
  'br': 'Brezhoneg',
  'ca': 'Català',
  'cs': 'Česky',
  'da': 'Dansk',
  'de': 'Deutsch',
  'el': 'Ελληνικά',
  'en': 'English',
  'es': 'Español',
  'et': 'Eesti',
  'fa': 'فارسی',
  'fr': 'Français',
  'he': 'עברית',
  'hrx': 'Hunsrik',
  'hu': 'Magyar',
  'ia': 'Interlingua',
  'is': 'Íslenska',
  'it': 'Italiano',
  'ja': '日本語',
  'kab': 'Kabyle',
  'ko': '한국어',
  'mk': 'Македонски',
  'ms': 'Bahasa Melayu',
  'nb': 'Norsk Bokmål',
  'nl': 'Nederlands, Vlaams',
  'oc': 'Lenga d\'òc',
  'pl': 'Polski',
  'pms': 'Piemontèis',
  'pt-br': 'Português Brasileiro',
  'ro': 'Română',
  'ru': 'Русский',
  'sc': 'Sardu',
  'sk': 'Slovenčina',
  'sr': 'Српски',
  'sv': 'Svenska',
  'ta': 'தமிழ்',
  'th': 'ภาษาไทย',
  'tlh': 'tlhIngan Hol',
  'tr': 'Türkçe',
  'uk': 'Українська',
  'vi': 'Tiếng Việt',
  'zh-hans': '简体中文',
  'zh-hant': '正體中文'
};

/**
 * List of RTL languages.
 */
Code.LANGUAGE_RTL = ['ar', 'fa', 'he', 'lki'];

/**
 * Blockly's main workspace.
 * @type {Blockly.WorkspaceSvg}
 */
Code.workspace = null;

/**
 * Extracts a parameter from the URL.
 * If the parameter is absent default_value is returned.
 * @param {string} name The name of the parameter.
 * @param {string} defaultValue Value to return if parameter not found.
 * @return {string} The parameter value or the default value if not found.
 */
Code.getStringParamFromUrl = function(name, defaultValue) {
  var val = location.search.match(new RegExp('[?&]' + name + '=([^&]+)'));
  return val ? decodeURIComponent(val[1].replace(/\+/g, '%20')) : defaultValue;
};

/**
 * Get the language of this user from the URL.
 * @return {string} User's language.
 */
Code.getLang = function() {
  var lang = Code.getStringParamFromUrl('lang', '');
  if (Code.LANGUAGE_NAME[lang] === undefined) {
    // Default to English.
    lang = 'en';
  }
  return lang;
};

/**
 * Is the current language (Code.LANG) an RTL language?
 * @return {boolean} True if RTL, false if LTR.
 */
Code.isRtl = function() {
  return Code.LANGUAGE_RTL.indexOf(Code.LANG) != -1;
};

/**
 * Load blocks saved on App Engine Storage or in session/local storage.
 * @param {string} defaultXml Text representation of default blocks.
 */
Code.loadBlocks = function(defaultXml) {
  try {
    var loadOnce = window.sessionStorage.loadOnceBlocks;
  } catch(e) {
    // Firefox sometimes throws a SecurityError when accessing sessionStorage.
    // Restarting Firefox fixes this, so it looks like a bug.
    var loadOnce = null;
  }
  if ('BlocklyStorage' in window && window.location.hash.length > 1) {
    // An href with #key trigers an AJAX call to retrieve saved blocks.
    BlocklyStorage.retrieveXml(window.location.hash.substring(1));
  } else if (loadOnce) {
    // Language switching stores the blocks during the reload.
    delete window.sessionStorage.loadOnceBlocks;
    var xml = Blockly.Xml.textToDom(loadOnce);
    Blockly.Xml.domToWorkspace(xml, Code.workspace);
  } else if (defaultXml) {
    // Load the editor with default starting blocks.
    var xml = Blockly.Xml.textToDom(defaultXml);
    Blockly.Xml.domToWorkspace(xml, Code.workspace);
  } else if ('BlocklyStorage' in window) {
    // Restore saved blocks in a separate thread so that subsequent
    // initialization is not affected from a failed load.
    window.setTimeout(BlocklyStorage.restoreBlocks, 0);
  }
};

/**
 * Save the blocks and reload with a different language.
 */
Code.changeLanguage = function() {
  // Store the blocks for the duration of the reload.
  // MSIE 11 does not support sessionStorage on file:// URLs.
  if (window.sessionStorage) {
    var xml = Blockly.Xml.workspaceToDom(Code.workspace);
    var text = Blockly.Xml.domToText(xml);
    window.sessionStorage.loadOnceBlocks = text;
  }

  var languageMenu = document.getElementById('languageMenu');
  var newLang = encodeURIComponent(
      languageMenu.options[languageMenu.selectedIndex].value);
  var search = window.location.search;
  if (search.length <= 1) {
    search = '?lang=' + newLang;
  } else if (search.match(/[?&]lang=[^&]*/)) {
    search = search.replace(/([?&]lang=)[^&]*/, '$1' + newLang);
  } else {
    search = search.replace(/\?/, '?lang=' + newLang + '&');
  }

  window.location = window.location.protocol + '//' +
      window.location.host + window.location.pathname + search;
};

/**
 * Bind a function to a button's click event.
 * On touch enabled browsers, ontouchend is treated as equivalent to onclick.
 * @param {!Element|string} el Button element or ID thereof.
 * @param {!Function} func Event handler to bind.
 */
Code.bindClick = function(el, func) {
  if (typeof el == 'string') {
    el = document.getElementById(el);
  }
  el.addEventListener('click', func, true);
  el.addEventListener('touchend', func, true);
};

/**
 * Load the Prettify CSS and JavaScript.
 */
Code.importPrettify = function() {
  var script = document.createElement('script');
  script.setAttribute('src', 'https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js');
  document.head.appendChild(script);
};

/**
 * Compute the absolute coordinates and dimensions of an HTML element.
 * @param {!Element} element Element to match.
 * @return {!Object} Contains height, width, x, and y properties.
 * @private
 */
Code.getBBox_ = function(element) {
  var height = element.offsetHeight;
  var width = element.offsetWidth;
  var x = 0;
  var y = 0;
  do {
    x += element.offsetLeft;
    y += element.offsetTop;
    element = element.offsetParent;
  } while (element);
  return {
    height: height,
    width: width,
    x: x,
    y: y
  };
};

/**
 * User's language (e.g. "en").
 * @type {string}
 */
Code.LANG = Code.getLang();

/**
 * List of tab names.
 * @private
 */
Code.TABS_ = ['blocks', 'python', 'xml'];

Code.selected = 'blocks';

/**
 * Switch the visible pane when a tab is clicked.
 * @param {string} clickedName Name of tab clicked.
 */
Code.tabClick = function(clickedName) {
	//Reset global variable for step increment
	//gStep = 0;

  // If the XML tab was open, save and render the content.
  if (document.getElementById('tab_xml').className == 'tabon') {
    var xmlTextarea = document.getElementById('content_xml');
    var xmlText = xmlTextarea.value;
    var xmlDom = null;
    try {
      xmlDom = Blockly.Xml.textToDom(xmlText);
    } catch (e) {
      var q =
          window.confirm(MSG['badXml'].replace('%1', e));
      if (!q) {
        // Leave the user on the XML tab.
        return;
      }
    }
    if (xmlDom) {
      Code.workspace.clear();
      Blockly.Xml.domToWorkspace(xmlDom, Code.workspace);
    }
  }

  if (document.getElementById('tab_blocks').className == 'tabon') {
    Code.workspace.setVisible(false);
  }
  // Deselect all tabs and hide all panes.
  for (var i = 0; i < Code.TABS_.length; i++) {
    var name = Code.TABS_[i];
	//console.log('tab name za skrivanje: ' + name);
    document.getElementById('tab_' + name).className = 'taboff';
    document.getElementById('content_' + name).style.visibility = 'hidden';
  }

  // Select the active tab.
  Code.selected = clickedName;
  document.getElementById('tab_' + clickedName).className = 'tabon';
  // Show the selected pane.
  //console.log('tab name za prikazivanje: ' + clickedName);
  document.getElementById('content_' + clickedName).style.visibility =
      'visible';
  Code.renderContent();
  if (clickedName == 'blocks') {
    Code.workspace.setVisible(true);
  }
  Blockly.svgResize(Code.workspace);
};

/**
 * Populate the currently selected pane with content generated from the blocks.
 */
Code.renderContent = function() {
  var content = document.getElementById('content_' + Code.selected);
  // Initialize the pane.
  if (content.id == 'content_xml') {
    var xmlTextarea = document.getElementById('content_xml');
    var xmlDom = Blockly.Xml.workspaceToDom(Code.workspace);
    var xmlText = Blockly.Xml.domToPrettyText(xmlDom);
    xmlTextarea.value = xmlText;
    xmlTextarea.focus();
  } else if (content.id == 'content_python') {
    Code.attemptCodeGeneration(Blockly.Python, 'py');
  }
};

/**
 * Attempt to generate the code and display it in the UI, pretty printed.
 * @param generator {!Blockly.Generator} The generator to use.
 * @param prettyPrintType {string} The file type key for the pretty printer.
 */
Code.attemptCodeGeneration = function(generator, prettyPrintType) {
  var content = document.getElementById('content_' + Code.selected);
  content.textContent = '';
  gImportCode = "";
  gHeaderCode = "";
  if (Code.checkAllGeneratorFunctionsDefined(generator)) {
    var code = generator.workspaceToCode(Code.workspace);

    content.textContent = gHeaderCode + '\n' + gImportCode + '\n' + code;
    if (typeof PR.prettyPrintOne == 'function') {
      code = content.textContent;
      code = PR.prettyPrintOne(code, prettyPrintType);
      content.innerHTML = code;
    }
  }
};

/**
 * Check whether all blocks in use have generator functions.
 * @param generator {!Blockly.Generator} The generator to use.
 */
Code.checkAllGeneratorFunctionsDefined = function(generator) {
  var blocks = Code.workspace.getAllBlocks();
  var missingBlockGenerators = [];
  for (var i = 0; i < blocks.length; i++) {
    var blockType = blocks[i].type;
    if (!generator[blockType]) {
      if (missingBlockGenerators.indexOf(blockType) === -1) {
        missingBlockGenerators.push(blockType);
      }
    }
  }

  var valid = missingBlockGenerators.length == 0;
  if (!valid) {
    var msg = 'The generator code for the following blocks not specified for '
        + generator.name_ + ':\n - ' + missingBlockGenerators.join('\n - ');
    Blockly.alert(msg);  // Assuming synchronous. No callback.
  }
  return valid;
};

/**
 * Initialize Blockly.  Called on page load.
 */
Code.init = function() {
  Code.initLanguage();
  var rtl = Code.isRtl();
  var container = document.getElementById('content_area');
  var onresize = function(e) {
    var bBox = Code.getBBox_(container);
    for (var i = 0; i < Code.TABS_.length; i++) {
      var el = document.getElementById('content_' + Code.TABS_[i]);
      el.style.top = bBox.y + 'px';
      el.style.left = bBox.x + 'px';
      // Height and width need to be set, read back, then set again to
      // compensate for scrollbars.
      el.style.height = bBox.height + 'px';
      el.style.height = (2 * bBox.height - el.offsetHeight) + 'px';
      el.style.width = bBox.width + 'px';
      el.style.width = (2 * bBox.width - el.offsetWidth) + 'px';
    }
    // Make the 'Blocks' tab line up with the toolbox.
    if (Code.workspace && Code.workspace.toolbox_.width) {
      document.getElementById('tab_blocks').style.minWidth =
          (Code.workspace.toolbox_.width - 38) + 'px';
          // Account for the 19 pixel margin and on each side.
    }
  };
  window.addEventListener('resize', onresize, false);

  // The toolbox XML specifies each category name using Blockly's messaging
  // format (eg. `<category name="%{BKY_CATLOGIC}">`).
  // These message keys need to be defined in `Blockly.Msg` in order to
  // be decoded by the library. Therefore, we'll use the `MSG` dictionary that's
  // been defined for each language to import each category name message
  // into `Blockly.Msg`.
  // TODO: Clean up the message files so this is done explicitly instead of
  // through this for-loop.
  for (var messageKey in MSG) {
    if (messageKey.indexOf('cat') == 0) {
      Blockly.Msg[messageKey.toUpperCase()] = MSG[messageKey];
    }
  }

  // Construct the toolbox XML, replacing translated variable names.
  var toolboxText = document.getElementById('toolbox').outerHTML;
  toolboxText = toolboxText.replace(/(^|[^%]){(\w+)}/g,
      function(m, p1, p2) {return p1 + MSG[p2];});
  var toolboxXml = Blockly.Xml.textToDom(toolboxText);

  Code.workspace = Blockly.inject('content_blocks',
      {grid:
          {spacing: 25,
           length: 3,
           colour: '#ccc',
           snap: true},
       media: '../../media/',
       rtl: rtl,
       toolbox: toolboxXml,
       zoom:
           {controls: true,
            wheel: true}
      });

  // Add to reserved word list: Local variables in execution environment (runJS)
  // and the infinite loop detection function.
  Blockly.JavaScript.addReservedWords('code,timeouts,checkTimeout');

  //Code.loadBlocks('');
  Code.loadBlocks('<xml xmlns="http://www.w3.org/1999/xhtml">\n' + defaultXMLHeader + defaultXMLFunctions + '</xml>');

  if ('BlocklyStorage' in window) {
    // Hook a save function onto unload.
    BlocklyStorage.backupOnUnload(Code.workspace);
  }

  Code.tabClick(Code.selected);

  Code.bindClick('trashButton',
      function() {Code.discard(); Code.renderContent();});
  Code.bindClick('runButton', Code.runJS);
  // Disable the link button if page isn't backed by App Engine storage.
  var linkButton = document.getElementById('linkButton');
  if ('BlocklyStorage' in window) {
    BlocklyStorage['HTTPREQUEST_ERROR'] = MSG['httpRequestError'];
    BlocklyStorage['LINK_ALERT'] = MSG['linkAlert'];
    BlocklyStorage['HASH_ERROR'] = MSG['hashError'];
    BlocklyStorage['XML_ERROR'] = MSG['xmlError'];
    Code.bindClick(linkButton,
        function() {BlocklyStorage.link(Code.workspace);});
  } else if (linkButton) {
    linkButton.className = 'disabled';
  }

  for (var i = 0; i < Code.TABS_.length; i++) {
    var name = Code.TABS_[i];
    Code.bindClick('tab_' + name,
        function(name_) {return function() {Code.tabClick(name_);};}(name));
  }
  onresize();
  Blockly.svgResize(Code.workspace);

  // Lazy-load the syntax-highlighting.
  window.setTimeout(Code.importPrettify, 1);




  //TEDD - PTC preview event listener
  Code.workspace.addChangeListener(PTC_preview_event_listener);

  //TEDD - Expected results preview event listener
  Code.workspace.addChangeListener(Expected_results_preview_event_listener);
};

/**
 * Initialize the page language.
 */
Code.initLanguage = function() {
  // Set the HTML's language and direction.
  var rtl = Code.isRtl();
  document.dir = rtl ? 'rtl' : 'ltr';
  document.head.parentElement.setAttribute('lang', Code.LANG);

  // Sort languages alphabetically.
  var languages = [];
  for (var lang in Code.LANGUAGE_NAME) {
    languages.push([Code.LANGUAGE_NAME[lang], lang]);
  }
  var comp = function(a, b) {
    // Sort based on first argument ('English', 'Русский', '简体字', etc).
    if (a[0] > b[0]) return 1;
    if (a[0] < b[0]) return -1;
    return 0;
  };
  languages.sort(comp);
  // Populate the language selection menu.
  //var languageMenu = document.getElementById('languageMenu');
  //languageMenu.options.length = 0;
  for (var i = 0; i < languages.length; i++) {
    var tuple = languages[i];
    var lang = tuple[tuple.length - 1];
    var option = new Option(tuple[0], lang);
    if (lang == Code.LANG) {
      option.selected = true;
    }
    //languageMenu.options.add(option);
  }
  //languageMenu.addEventListener('change', Code.changeLanguage, true);

  // Inject language strings.
  //document.title += ' ' + MSG['title'];
  //document.getElementById('title').textContent = MSG['title'];
  //document.getElementById('tab_blocks').textContent = MSG['blocks'];

  //document.getElementById('linkButton').title = MSG['linkTooltip'];
  //document.getElementById('runButton').title = MSG['runTooltip'];
  //document.getElementById('trashButton').title = MSG['trashTooltip'];
};

/**
 * Execute the user's code.
 * Just a quick and dirty eval.  Catch infinite loops.
 */
Code.runJS = function() {
  Blockly.JavaScript.INFINITE_LOOP_TRAP = '  checkTimeout();\n';
  var timeouts = 0;
  var checkTimeout = function() {
    if (timeouts++ > 1000000) {
      throw MSG['timeout'];
    }
  };
  var code = Blockly.JavaScript.workspaceToCode(Code.workspace);
  Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
  try {
    eval(code);
  } catch (e) {
    alert(MSG['badCode'].replace('%1', e));
  }
};

/**
 * Discard all blocks from the workspace.
 */
Code.discard = function() {
  var count = Code.workspace.getAllBlocks().length;
  if (count < 2 ||
      window.confirm(Blockly.Msg.DELETE_ALL_BLOCKS.replace('%1', count))) {
    Code.workspace.clear();
    if (window.location.hash) {
      window.location.hash = '';
    }
  }
};

// Load the Code demo's language strings.
document.write('<script src="msg/' + Code.LANG + '.js"></script>\n');
// Load Blockly's language strings.
document.write('<script src="../../msg/js/' + Code.LANG + '.js"></script>\n');

window.addEventListener('load', Code.init);

//My Functions
function toJS() {
  alert("Saving code to JS file!");
  var content = document.getElementById('content_javascript');
  download(content.textContent, 'test.js', 'js');
}

function saveJSToFile(){
  if ('Blob' in window) {
    var fileName = prompt('Please enter file name to save', 'Untitled.js');
    if (fileName) {
      var textToWrite = document.getElementById('content_javascript').textContent.replace(/\n/g, '\r\n');
      var textFileAsBlob = new Blob([textToWrite], { type: 'js' });

      if ('msSaveOrOpenBlob' in navigator) {
        navigator.msSaveOrOpenBlob(textFileAsBlob, fileName);
      } else {
        var downloadLink = document.createElement('a');
        downloadLink.download = fileName;
        downloadLink.innerHTML = 'Download File';
        if ('webkitURL' in window) {
          // Chrome allows the link to be clicked without actually adding it to the DOM.
          downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
        } else {
          // Firefox requires the link to be added to the DOM before it can be clicked.
          downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
          downloadLink.onclick = destroyClickedElement;
          downloadLink.style.display = 'none';
          document.body.appendChild(downloadLink);
        }

        downloadLink.click();
      }
    }
  } else {
    alert('Your browser does not support the HTML5 Blob.');
  }
};

function savePyToFile(){
  if ('Blob' in window) {
    var fileName = prompt('Please enter file name to save', 'Untitled.py');
    if (fileName) {
      var textToWrite = document.getElementById('content_python').textContent.replace(/\n/g, '\r\n');
      var textFileAsBlob = new Blob([textToWrite], { type: 'py' });

      if ('msSaveOrOpenBlob' in navigator) {
        navigator.msSaveOrOpenBlob(textFileAsBlob, fileName);
      } else {
        var downloadLink = document.createElement('a');
        downloadLink.download = fileName;
        downloadLink.innerHTML = 'Download File';
        if ('webkitURL' in window) {
          // Chrome allows the link to be clicked without actually adding it to the DOM.
          downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
        } else {
          // Firefox requires the link to be added to the DOM before it can be clicked.
          downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
          downloadLink.onclick = destroyClickedElement;
          downloadLink.style.display = 'none';
          document.body.appendChild(downloadLink);
        }

        downloadLink.click();
      }
    }
  } else {
    alert('Your browser does not support the HTML5 Blob.');
  }
};

// HELPER FUNCTIONS

// returns siblings of a DOM node
function getSiblings(el) {
    var siblings = [];
    while (el = el.nextSibling)
	{
		siblings.push(el);
	}
    return siblings;
}

// recursively find the bottom block of the workspace blocks DOM tree
function recursiveDomBottom(blockNode) {
	// find the "next" node if there is one
	var blockChildren = blockNode.childNodes;
	for (var i = 0; i < blockChildren.length; i++)
	{
		var nodeNamer = blockChildren[i].nodeName.toUpperCase();
		if (nodeNamer == "NEXT")
		{
			return recursiveDomBottom(blockChildren[i].firstChild);
		}

	}

	return blockNode;
}

function nestBlockIntoAnotherBlock(blockName)
{
	// get workspace DOM
	var workspaceDom = Blockly.Xml.workspaceToDom(Code.workspace);

	// find the newly added block (it is always at the end, since we are adding it to the workspace via appendDomToWOrkspace
	var topBlocks = workspaceDom.childNodes;
	var newlyAddedBlock = topBlocks[topBlocks.length - 1];

	// check whether coordinates should be removed from the block, since it will be nested
	// REMOVE AFTERWARDS
	newlyAddedBlock.removeAttribute("x");
	newlyAddedBlock.removeAttribute("y");

	// get the "field" DOM child of the measurement block (traverse through all field DOM objects in all the blocks)
	// we assume it exists (#BUG: if someone deletes the measurement block?)
	var fieldNames = workspaceDom.querySelectorAll("field[name='NAME']");
	var fieldName = null;
	for (var i = 0; i < fieldNames.length; i++)
	{
		// if the "field" dom node is found in the measurement block, return that specific one
		if ((fieldNames[i].innerHTML).toUpperCase() == blockName)
		{
			fieldName = fieldNames[i];
		}
	}
	// get the measurement block DOM node (needed later)
	var measurementBlock = fieldName.parentNode;

	// get siblings of the "field" DOM node and check if there is a node named "STACK" within (indicating that measurement block has blocks inside)
	var sibs = getSiblings(fieldName);
	//console.log(sibs);
	var stack = null;
	for (var i = 0; i < sibs.length; i++)
	{
		if ((sibs[i].nodeName.toUpperCase() == "STATEMENT") && (sibs[i].getAttribute('name').toUpperCase() == "STACK"))
		{
			stack = sibs[i];
		}
	}

	// if there is no statement node with the name STACK, create it and mark it as a node to which the added block should be moved
	// this also indicates that there is no need for block nesting
	if (stack == null)
	{
		var newStack = document.createElement("statement");
		newStack.setAttribute("name", "STACK");
		measurementBlock.appendChild(newStack);
		stack = newStack;

		// move the newly added block to the newly created DOM node "STACK" of the measurement block
		stack.appendChild(newlyAddedBlock);
	}
	// if the dom node STACK exists
	// 		- check if there are already nested blocks within
	//		- if not add block directly in STACK
	else
	{
		// if stack is empty (but it exists) - it happens when blocks from measurement top block are removed
		if (!(stack.firstChild))
		{
			// add the block directly
			// move the newly added block to the newly created DOM node "STACK" of the measurement block
			stack.appendChild(newlyAddedBlock);
		}
		else
		{
			// recursively find the bottom block
			var lastBlockInTree = recursiveDomBottom(stack.firstChild);
			console.log(lastBlockInTree);
			// create the DOM node named next
			var nextNode = document.createElement("next");
			// put the DOM node named next in the bottom block
			lastBlockInTree.appendChild(nextNode);
			// move the newly added block to the newly created DOM node "next" of the bottom block
			nextNode.appendChild(newlyAddedBlock);
		}
	}

	// now when the DOM representation of the workspace is completed, apply it
	Code.workspace.clear();
	Blockly.Xml.domToWorkspace(workspaceDom, Code.workspace);

	//console.log(workspaceDom);
}


function insertFirstBlock(selectedCategory, blockName)
{
	// console.log("Selected category in inserting the block: " + selectedCategory.getAttribute("name"));
	// get blocks from the selected category
	var blocks = selectedCategory.getElementsByTagName("block");
	var firstBlock = blocks[0];
	// if no blocks in the search results do nothing
	if (firstBlock == null)
	{
		return;
	}
	// else add the first block to the workspace
	else
	{
		var firstBlockXml = Blockly.Xml.domToText(firstBlock);
		var xml = Blockly.Xml.textToDom('<xml>' + firstBlockXml + '</xml>');
		// console.log(xml);

		// append block to the workspace
		Blockly.Xml.appendDomToWorkspace(xml, Code.workspace);

		// get the top blocks as block objects
		var topBlocks = Code.workspace.getTopBlocks();
		// get the newly added block as the block object (it is last in the list)
		var addedBlock = topBlocks[topBlocks.length - 1];
		console.log(addedBlock.nextConnection);
		console.log(addedBlock.previousConnection);
		console.log(addedBlock);
		console.log(addedBlock.getInputsInline() + ": " + addedBlock.getInputsInline());

		// if the block has top and bottom connections reallocate it to the measurement block
		if ( (addedBlock.nextConnection != null) && (addedBlock.previousConnection != null) )
			nestBlockIntoAnotherBlock(blockName);
	}
}

// searches for the category named Search results and returns it. Returns null if not found (should never happen).
function getSearchResultsCategory(categories)
{
	for (var i = 0; i < categories.length; i++)
	{
		if (categories[i].getAttribute("name") == "Search results")
			return categories[i];
	}
	return null;
}

function getCategories(toolbox)
{
	var categories = toolbox.getElementsByTagName("category");
	return categories;
}

function getActiveCategory(categoriesTemp)
{
	// OBSOLETE: :0 is the container, :1 is the first category, first check if a category is selected
	// The numbers change when a toolbox is reinjected
	// var toolBoxtree = document.getElementById(':0')
	var toolboxTrees = document.getElementsByClassName("blocklyTreeRoot");
	// #POSSIBLE BUG - if there are multiple elements having the class blocklyTreeRoot BEFORE the one we need this fails!
	if (toolboxTrees[0].hasAttribute('aria-activedescendant'))
	{

		// if a category is selected, get it
		var activeCategoryId = toolboxTrees[0].getAttribute('aria-activedescendant');

		var activeCategoryDiv = document.getElementById(activeCategoryId);

		var activeCategoryName = activeCategoryDiv.querySelector('[class="blocklyTreeLabel"]').innerHTML;


		for (var i = 0; i < categoriesTemp.length; i++)
		{
			//console.log(categories[i].getAttribute("name"));
			if (categoriesTemp[i].getAttribute("name") == activeCategoryName)
				return categoriesTemp[i];
		}
		return getSearchResultsCategory(categoriesTemp); // returns default category if none selected
	}
	else return getSearchResultsCategory(categoriesTemp); // returns default category if none selected
}

// return the next attached block (by Blockly rules) or null
function returnNextAttachedBlock(blockNode)
{
	var childNodeArray = blockNode.childNodes;
	// if it has no children
	if ( !(childNodeArray) ) return null;
	else
	{
		for (var i = 0; i < childNodeArray.length; i++)
		{
			// if it has a child node named next
			if (childNodeArray[i].nodeName.toUpperCase() == "NEXT")
			{
				var childrenOfNext = childNodeArray[i].childNodes;
				for (var j = 0; j < childrenOfNext.length; j++)
				{
					if (childrenOfNext[j].nodeName.toUpperCase() == "BLOCK") return childrenOfNext[j];
				}
			}
		}
	}
	return null;
}

// recursively find a block attached to the import blocks (recursion because of possible multiple imports for a signle attached block)
function getBlockToFilter(blockNode)
{
	// get the block type
	var blockType = blockNode.getAttribute("type").toUpperCase();
	// get the next node or null
	var nodeNext = returnNextAttachedBlock(blockNode);
	// if it is not an import block or it has no attached blocks
	if ( (blockType.indexOf("IMPORT") == -1) || (nodeNext == null) )
		return blockNode;
	else
	{
		return getBlockToFilter(nodeNext);
	}
	return blockNode;
}

function filter()
{
	console.log("Event key pressed: " + event.key);
	// save the workspace
	var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
	var workspaceDom = Blockly.Xml.domToPrettyText(xml);

	// filter out the toolbox
	var searchText = document.getElementById("searchBox");
	console.log("Search query: " + searchText.value);
	var filter = searchText.value.toUpperCase();
	// restore original toolbox (copy the original one to the working copy)
	document.getElementById("toolboxWorkingCopy").innerHTML = document.getElementById("toolbox").innerHTML;
	var toolbox = document.getElementById("toolboxWorkingCopy");

	//get an array of category objects
	var categories = getCategories(toolbox);

	// get active category to remember which one has to be expanded after reinjecting
	var selectedCategory = getActiveCategory(categories);

	// #BUG (Vidovic) - if we delete a search query we want to display ALL the blocks (without limitation to 10), questionable if a lot of search results
	if (searchText.value != "")
	{
		// filter the results
		for (var i = 0; i < categories.length; i++)
		{
			var blocks = [];
						
			var blocksAll = categories[i].getElementsByTagName("block");
			
			// select only the top level blocks (not the nested ones)
			for (var j = 0; j < blocksAll.length; j++)
			{
				if (blocksAll[j].parentNode == categories[i])
				{
					blocks.push(blocksAll[j]);
				}
			}

			// no empty categories, so no need to check "if (blocks)"
			// has to go from blocks.length, otherwise the blocks.length is decremented in each iteration that removes a child
			// #BUG upgrade 5.9.2018. filter does not work for blocks having import attached (because filter uses name and the tooltip of the import block, not of the one attached)
			for (var j = blocks.length - 1; j >= 0; j--) {
				// initialize a currently chosen block
				var chosenBlock = blocks[j];
				
				// if it is an import block with attached blocks, select its nested block(s), otherwise use the current block
				chosenBlock = getBlockToFilter(blocks[j]);

				var currentTooltip = chosenBlock.getAttribute("tooltip");
				if (currentTooltip == null) currentTooltip = ' ';
				if ( (chosenBlock.getAttribute("type").toUpperCase().indexOf(filter) == -1) && (currentTooltip.toUpperCase().indexOf(filter) == -1) )
				{
					blocks[j].parentNode.removeChild(blocks[j]);
				}
			}

			// since some blocks were removed from DOM, they has to be removed from the arrays also, therefore clear the arrays and populate them again with the updated DOM
			blocks = [];
			blocksAll = [];

			blocksAll = categories[i].getElementsByTagName("block");
			for (var j = 0; j < blocksAll.length; j++)
			{
				if (blocksAll[j].parentNode == categories[i])
				{
					blocks.push(blocksAll[j]);
				}
			}

			// if more than gMaxSearchResults blocks, display only first gNumberOfDisplayedBlocks ones!
			if (blocks.length > gMaxSearchResults)
			{
				// 1. extract parent node (category) to add a label
				var parentBlock = blocks[0].parentNode;
				// display only first gNumberOfDisplayedBlocks blocks (if we want all, replace gNumberOfDisplayedBlocks with 0)
				for (var j = blocks.length - 1; j >= gNumberOfDisplayedBlocks; j--) {
					blocks[j].parentNode.removeChild(blocks[j]);
				}

				/*for (var j = blocks.length - 1; j >= 0; j--) {
					blocks[j].parentNode.removeChild(blocks[j]);
				}*/

				// create XML element <label> as a DOM element: to append to a flyout
				var searchErrorLabel = document.createElement('label');
				searchErrorLabel.setAttribute('text','Too many search results...');
				searchErrorLabel.setAttribute('web-class','myLabelStyle');
				parentBlock.appendChild(searchErrorLabel);
			}
		}

	}

	Code.workspace.dispose();
	selectedCategory.setAttribute("expanded", "true");

	// Construct the toolbox XML, replacing translated variable names.
	var toolboxText = document.getElementById('toolboxWorkingCopy').outerHTML;
	toolboxText = toolboxText.replace(/(^|[^%]){(\w+)}/g,
	function(m, p1, p2) {return p1 + MSG[p2];});
	var toolboxXml = Blockly.Xml.textToDom(toolboxText);


	// if rtl property is set, the TEDD crashes when searching (rtl variable which holds the value is not in scope here
	Code.workspace = Blockly.inject('content_blocks',
		{grid:
			{spacing: 25,
			length: 3,
			colour: '#ccc',
			snap: true},
		media: '../../media/',
		zoom:
			{controls: true,
			wheel: true},
		//rtl: rtl,
		toolbox: toolboxXml});

	// restore the workspace
	var restoreWorkspace = Blockly.Xml.textToDom(workspaceDom);
	Blockly.Xml.domToWorkspace(restoreWorkspace, Blockly.mainWorkspace);

	//TEDD - PTC preview event listener
	Code.workspace.addChangeListener(PTC_preview_event_listener);

	//TEDD - Expected results preview event listener
	Code.workspace.addChangeListener(Expected_results_preview_event_listener);

	// if enter is pressed, insert the first block into the measurement block in the workspace
	// ALT must not be pressed (because we are using it for another purpose below)
	if (event.keyCode == 13 && !(event.altKey))
	{
		insertFirstBlock(selectedCategory, "MEASUREMENT");
	}

	// if ALT + enter is pressed, insert the first block into the preconditions block in the workspace
	if (event.keyCode == 13 && event.altKey)
	{
		console.log("ALT+Enter pressed!");
		insertFirstBlock(selectedCategory, "PRECONDITIONS");
		// click the button that does the required function
		//document.getElementById("searchButton").click();
	}
};

function destroyClickedElement(event) {
  document.body.removeChild(event.target);
}

function test_listener(event) {
alert("THE MAIN LISTENER");
}

function PTC_preview_event_listener(event) {
  gStepPTC = 0;
  var code = Blockly.Lua.workspaceToCode(Code.workspace);
  //alert('Event listener started');
  //alert(code);
  document.getElementById('PTC_preview').innerHTML = code;
}

function Expected_results_preview_event_listener(event) {
	gStep = 0;
	gStepER = 0;
  var code = Blockly.PHP.workspaceToCode(Code.workspace);
  //alert('Event listener started');
  //alert(code);
  document.getElementById('Expected_results_preview').innerHTML = code;
}

function AddImportBlock(import_string)
{
  var allBlocks = Code.workspace.getAllBlocks(true);

  var addInclude = true;
  var blockType;
  var field;

  for(var iBlock in allBlocks)
  {
	  blockType = allBlocks[iBlock].type

	  if(blockType == "import_custom")
	  {
		  if(allBlocks[iBlock].getFieldValue("import_value") == import_string)
		  {
			  addInclude = false;
			  break;
		  }
	  }
  }

  if(addInclude)
  {
	  //var xml = Blockly.Xml.textToDom('<xml><block type="import_custom" x="100" y="' + gAddImportY +'"><field name="import_value">' + import_string + '</field></block></xml>');

	  var xml = Blockly.Xml.textToDom('<xml><block type="import_custom" x="700" y="50"><field name="import_value">' + import_string + '</field></block></xml>');
	  Blockly.Xml.domToWorkspace(xml, Code.workspace);

	  //Code.workspace.zoomToFit();

	  //gAddImportY += 40;
  }
}

function PreconditionsCheckBoxVisibility(block)
{
  var rootBlock = block.getRootBlock();
  var rootName = "";
  if(rootBlock)
	rootName = rootBlock.getFieldValue("NAME");

  var checkbox_field = block.getField('positive_check');

  if(rootName == "Preconditions")
  {
	checkbox_field.setVisible(false);
  }
  else
  {
	checkbox_field.setVisible(true);
  }
}

function CreateCode(block, preconditionCode, executionCode)
{
  var rootBlock = block.getRootBlock();
  var rootName = "";
  if(rootBlock)
	rootName = rootBlock.getFieldValue("NAME");

  var code;

  if(rootName == "Preconditions")
  {
	  code = preconditionCode;
  }
  else
  {
	  code = executionCode;
  }
	return code;
}

function CreateCode(block, preconditionCode, executionCode)
{
  var rootBlock = block.getRootBlock();
  var rootName = "";
  if(rootBlock)
	rootName = rootBlock.getFieldValue("NAME");

  var code;

  if(rootName == "Preconditions")
  {
	  code = preconditionCode;
  }
  else
  {
	  code = executionCode;
  }
	return code;
}

function AddHeaderBlock()
{
  if(gUpdateHeaderBlock)
  {
    var headerBlock = Code.workspace.getBlockById('bPBJK;|NC`{q|=O9evD}');

    if(headerBlock)
      headerBlock.dispose(true, true);

    var XMLHeader 	= 	'\
    <xml xmlns="http://www.w3.org/1999/xhtml">\n\
      <variables></variables>\n\
      <block type="tedd_header" id="bPBJK;|NC`{q|=O9evD}" x="50" y="50">\n\
        <mutation items="' + gHeaderItemsNumber + '"></mutation>\n\
        <value name="ADD0">\n\
          <block type="tedd_header_new_item">\n\
            <value name="entry_name">\n\
              <block type="text">\n\
                <field name="TEXT">Project name</field>\n\
              </block>\n\
            </value>\n\
            <value name="entry_value">\n\
              <block type="text">\n\
                <field name="TEXT">' + gProjectName + '</field>\n\
              </block>\n\
            </value>\n\
          </block>\n\
        </value>\n\
        <value name="ADD1">\n\
          <block type="tedd_header_new_item">\n\
            <value name="entry_name">\n\
              <block type="text">\n\
                <field name="TEXT">Project description</field>\n\
              </block>\n\
            </value>\n\
            <value name="entry_value">\n\
              <block type="text">\n\
                <field name="TEXT">' + gProjectDescription + '</field>\n\
              </block>\n\
            </value>\n\
          </block>\n\
        </value>\n\
        <value name="ADD2">\n\
          <block type="tedd_header_new_item" id="uh#ak=TlTsoxv%/}w4,d">\n\
            <value name="entry_name">\n\
              <block type="text" id="/1ZhRf2E(ZGAMO=Ha)*^">\n\
                <field name="TEXT">PTC_ID</field>\n\
              </block>\n\
            </value>\n\
            <value name="entry_value">\n\
              <block type="math_number" id="jlg:I$=as)4uCJ5/}^c1">\n\
                <field name="NUM">' + gPTCID + '</field>\n\
              </block>\n\
            </value>\n\
          </block>\n\
        </value>\n\
        <value name="ADD3">\n\
          <block type="tedd_header_new_item" id="$rqz9-}J]qFh,I1)w}1w">\n\
            <value name="entry_name">\n\
              <block type="text" id="N7THE16)DEd[N2ne2I~9">\n\
                <field name="TEXT">build</field>\n\
              </block>\n\
            </value>\n\
            <value name="entry_value">\n\
              <block type="text" id="y^Q!QrNtYXX2EfI427$_">\n\
                <field name="TEXT">' + gBuild + '</field>\n\
              </block>\n\
            </value>\n\
          </block>\n\
        </value>\n\
        <value name="ADD4">\n\
          <block type="tedd_header_new_item" id="L3k*7wb09+R$g*H7D[#*">\n\
            <value name="entry_name">\n\
              <block type="text" id="_HFzR57dD=frVEO41vkf">\n\
                <field name="TEXT">data set</field>\n\
              </block>\n\
            </value>\n\
            <value name="entry_value">\n\
              <block type="text" id="+;+@xyeXOa04SI0a25DQ">\n\
                <field name="TEXT">' + gData_set + '</field>\n\
              </block>\n\
            </value>\n\
          </block>\n\
        </value>\n';

    var xml = Blockly.Xml.textToDom(XMLHeader + gXMLHeaderConfigItems + '</block>\n</xml>');

    //Blockly.Xml.appendDomToWorkspace(xml, Code.workspace);
    Blockly.Xml.domToWorkspace(xml, Code.workspace);

    gXMLHeaderConfigItems = '';
    gUpdateHeaderBlock = 0;
  }
}

function headerNewItem(number, fieldName, filedType, fieldValue)
{
  var _fieldName = filedType == 'number' ? 'NUM' : 'TEXT';
  filedType = filedType == 'number' ? 'math_number' : 'text';

  gXMLHeaderConfigItems += '\
  <value name="ADD' + number +'">\n\
    <block type="tedd_header_new_item">\n\
      <value name="entry_name">\n\
        <block type="text">\n\
          <field name="TEXT">' + fieldName + '</field>\n\
        </block>\n\
      </value>\n\
      <value name="entry_value">\n\
        <block type="' + filedType + '">\n\
          <field name="' + _fieldName + '">' + fieldValue + '</field>\n\
        </block>\n\
      </value>\n\
    </block>\n\
  </value>\n';
}


function headerNewList(number, listItemJSON)
{
  var _xml;
  var _xmlItems = '';
  var listItems = listItemJSON.length;

  var i;
  for (i = 0; i < listItemJSON.length; i++) {
    var _fieldName = listItemJSON[i].type == 'number' ? 'NUM' : 'TEXT';
    var fieldType = listItemJSON[i].type == 'number' ? 'math_number' : 'text';


    _xmlItems += '\
    <value name="ADD' + i +'">\n\
      <block type="tedd_header_new_item">\n\
        <value name="entry_name">\n\
          <block type="text">\n\
            <field name="TEXT">' + listItemJSON[i].name + '</field>\n\
          </block>\n\
        </value>\n\
        <value name="entry_value">\n\
          <block type="' + fieldType + '">\n\
            <field name="' + _fieldName + '">' + listItemJSON[i].value + '</field>\n\
          </block>\n\
        </value>\n\
      </block>\n\
    </value>\n';

    //alert('i: ' + i + 'name: ' + listItemJSON[i].name + 'type: ' + fieldType + 'fName: ' + _fieldName + 'value: ' + listItemJSON[i].value);
  }


  _xml = '\
  <value name="ADD' + number + '">\n\
      <block type="tedd_header_new_item">\n\
        <value name="entry_name">\n\
          <block type="text">\n\
            <field name="TEXT">list_name</field>\n\
          </block>\n\
        </value>\n\
        <value name="entry_value">\n\
          <block type="lists_create_with">\n\
            <mutation items="' + listItems + '"></mutation>\n'
            + _xmlItems +
          '</block>\n\
        </value>\n\
      </block>\n\
    </value>';

    //alert(_xml);

  gXMLHeaderConfigItems += _xml;
}


var defaultXMLHeader 	= 	'\
  <block type="tedd_header" id="bPBJK;|NC`{q|=O9evD}" x="50" y="50">\n\
    <mutation items="3"></mutation>\n\
    <value name="ADD0">\n\
      <block type="tedd_header_new_item" id="uh#ak=TlTsoxv%/}w4,d">\n\
        <value name="entry_name">\n\
          <block type="text" id="/1ZhRf2E(ZGAMO=Ha)*^">\n\
            <field name="TEXT">PTC_ID</field>\n\
          </block>\n\
        </value>\n\
        <value name="entry_value">\n\
          <block type="math_number" id="jlg:I$=as)4uCJ5/}^c1">\n\
            <field name="NUM">' + gPTCID + '</field>\n\
          </block>\n\
        </value>\n\
      </block>\n\
    </value>\n\
    <value name="ADD1">\n\
      <block type="tedd_header_new_item" id="$rqz9-}J]qFh,I1)w}1w">\n\
        <value name="entry_name">\n\
          <block type="text" id="N7THE16)DEd[N2ne2I~9">\n\
            <field name="TEXT">build</field>\n\
          </block>\n\
        </value>\n\
        <value name="entry_value">\n\
          <block type="text" id="y^Q!QrNtYXX2EfI427$_">\n\
            <field name="TEXT">' + gBuild + '</field>\n\
          </block>\n\
        </value>\n\
      </block>\n\
    </value>\n\
    <value name="ADD2">\n\
      <block type="tedd_header_new_item" id="L3k*7wb09+R$g*H7D[#*">\n\
        <value name="entry_name">\n\
          <block type="text" id="_HFzR57dD=frVEO41vkf">\n\
            <field name="TEXT">data set</field>\n\
          </block>\n\
        </value>\n\
        <value name="entry_value">\n\
          <block type="text" id="+;+@xyeXOa04SI0a25DQ">\n\
            <field name="TEXT">' + gData_set + '</field>\n\
          </block>\n\
        </value>\n\
      </block>\n\
    </value>\n\
  </block>\n';


var defaultXMLFunctions 	= 	'\
  <block type="procedures_defnoreturn" id="fG3qFPG:jR~CZhG{LQLo" x="50" y="600">\n\
    <field name="NAME">Preconditions</field>\n\
    <comment pinned="false" h="80" w="160"></comment>\n\
  </block>\n\
  <block type="procedures_defnoreturn" id="EEX(ppO0?u.c{$g6;loM" x="50" y="800">\n\
    <field name="NAME">Measurement</field>\n\
    <comment pinned="false" h="80" w="160"></comment>\n\
  </block>\n';
