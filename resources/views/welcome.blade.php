<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TEDD Demo</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">


		<link rel="stylesheet" href="/css/tedd.css">
		<script src="blockly_compressed.js"></script>
		<script src="blocks_compressed.js"></script>
		<script src="javascript_compressed.js"></script>
		<script src="python_compressed.js"></script>
		<script src="lua_compressed.js"></script>
		<script src="php_compressed.js"></script>
		<script src="msg/en.js"></script>
		<script src="TEDD/CoreJSFunctions.js"></script>
		<script src="scripts/core.js"></script>
    <script src="scripts/autocomplete.js"></script>
		<script src="TEDD/TEDD_Blocks.js"></script>
		<script src="TEDD/TEDD_Blocks_generator.js"></script>
		<script src="TEDD/TEDD_Blocks_PTC_generator.js"></script>
		<script src="TEDD/TEDD_Blocks_ER_generator.js"></script>

    </head>

    <!-- TEDD -->
    <?php
    $configFiles = [];
    $configFilePaths = File::files(storage_path() . '/tedd/projectConfigFiles');
    //print_r($configFilePaths);


    foreach ($configFilePaths as $key => $file) {
      $configFiles[] = pathinfo($file);

      //echo $configFiles[$key]['filename'];
      //echo $configFiles[$key]['dirname'];
    }

    //Get all test case files
    $testCaseFiles = [];
    $testCaseFilesDisplay = "[";
    $testCaseFilePaths = File::files(storage_path() . '/tedd/testCaseFiles');
    //print_r($configFilePaths);


    foreach ($testCaseFilePaths as $key => $file) {
      $testCaseFiles[] = pathinfo($file);
      $testCaseFilesDisplay .= '"' . $testCaseFiles[$key]['filename'] . '",';
      //$testCaseFilesDisplay = $testCaseFiles[$key]['filename'];

      //echo $configFiles[$key]['filename'];
      //echo $configFiles[$key]['dirname'];
    }

    $testCaseFilesDisplay .= "]";
    //echo $testCaseFilesDisplay;

    //$contents = File::get(storage_path('tedd/testCaseFiles/123.xml'));
    //echo storage_path('123.xml');
    //header('Content-Type: application/xml');
    //echo nl2br(str_replace('<', '&lt;', $contents));

    //$xml = new DomDocument('1.0', 'utf-8'); // Or the right version and encoding of your xml file
    //$xml->load(storage_path('tedd/testCaseFiles/123.xml'));
    //print_r($xml);
    //console.log($contents);
    //echo 'PHP: ' . phpinfo();

    $command = escapeshellcmd(storage_path() . "/tedd/testPython.py");
    //echo storage_path() . "/tedd/testPython.py";
    $output = shell_exec($command);
    echo 'skripta rezultat: ' . $output;

    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
        $('#TEDDProjectSelect').change(function(){
            //Selected value
            var gProjectConfigFile = $(this).val();
            //alert("value in js " + gProjectConfigFile);

            if(gProjectConfigFile != 'none')
            {
              $.get('getRequest', {data: gProjectConfigFile}, function(data){
                //$('#getRequestData').append(data);
                console.log(data);

                gUpdateHeaderBlock = 1;

                var jsonObject = JSON.parse(data);

                console.log(jsonObject);
                gHeaderItemsNumber = 5 + jsonObject[0].header.length;
                gProjectName = jsonObject[0].projectName;
                gProjectDescription = jsonObject[0].description;
                gBuild = jsonObject[0].build;
                gData_set = jsonObject[0].dataSet;

                var i;
                for (i = 0; i < jsonObject[0].header.length; i++) {
                    if(jsonObject[0].header[i].type == 'list')
                      headerNewList((i+5), jsonObject[0].header[i].items);
                    else
                      headerNewItem((i+5), jsonObject[0].header[i].name, jsonObject[0].header[i].type, jsonObject[0].header[i].value);
                }
              });
            }
            /*else {
              gUpdateHeaderBlock = 0;
            }*/
        });


        $('#idTestCaseOpenBTN').click(function(){
            //Selected value
            var testCaseFile = $('#idTestCaseOpen').val();
            //alert("value in js " + testCaseFile);

            var validNames = <?= $testCaseFilesDisplay ?>;
            if(validNames.indexOf(testCaseFile) > -1)
            {
              //alert('Valid PTC ID!');

              $.get('loadTestCase', {data: testCaseFile}, function(data){
                //$('#getRequestData').append(data);
                console.log(data);
                Code.workspace.clear();
                var xml = Blockly.Xml.textToDom(data);
                Blockly.Xml.domToWorkspace(xml, Code.workspace);
              });
            }
            else {
              //alert('Invalid PTC ID');
            }
        });

        $('#idTestCaseSaveBTN').click(function(){
            //Selected value
            //var testCaseFile = $('#idTestCaseOpen').val();
            //alert("current PTC ID is: " + gPTCID);
            var fileName = "";

            if(!isNaN(gPTCID))
            {
              //alert('PTC ID is a number!');
              fileName = gPTCID;
            }
            else {
              //alert('PTC ID is not a number!');
              var fileName = "";
              var i = 0;
              while(fileName == "")
              {
                if(!i++)
                  fileName = prompt("Please enter file name:", "");
                else
                  fileName = prompt("File name can not be blank!", "");
              }
            }

            if(fileName != null)
            {
              var xml = Blockly.Xml.workspaceToDom(Code.workspace);
              var text = Blockly.Xml.domToText(xml);
              //alert(text);

              $.get('saveTestCase', {data: fileName, xml: text }, function(data){
                //$('#getRequestData').append(data);
                alert('Test case saved!');
                console.log(data);
                /*Code.workspace.clear();
                var xml = Blockly.Xml.textToDom(data);
                Blockly.Xml.domToWorkspace(xml, Code.workspace);*/
              });
            }
        });

    });
    </script>

    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif
		</div>

		<h1>TEDD</h1>

    <table width = "100%">
      <tr>
        <td>
          Project:
          <select id="TEDDProjectSelect" name="TEDDProject">
              <?php
              echo '<option value="none">Select TEDD project...</option>';
              foreach ($configFiles as $key => $file) {
                echo '<option value="' . $configFiles[$key]['basename']  . '">' . $configFiles[$key]['filename'] . '</option>';
              }
              ?>
          </select>
          <button id="idApplyConfigFile" onclick="AddHeaderBlock();"><img width="10px" height="100%" src='media/apply2.png'></button>
          <!-- <input id="idApplyConfigFile" type="button" value="Apply" onclick="AddHeaderBlock();"> -->
        </td>

          <td style="text-align:right;">
            <input id="idTestCaseOpen" list="testCaseFiles" autocomplete=off>

            <?php
            echo '<datalist id="testCaseFiles">';
            foreach ($testCaseFiles as $key => $file) {
              echo '<option>' . $testCaseFiles[$key]['filename'] .'</option>';
            }
            echo '</datalist>';
            ?>

            <button id="idTestCaseOpenBTN"><img width="10px" height="100%" src='media/open.png'></button>
            <button id="idTestCaseSaveBTN"><img width="10px" height="100%" src='media/save.png'></button>
          </td>
      </tr>
    </table>

    <hr>


		<table>
			<tr id="tabRow" height="1em">
				<td id="tab_blocks" class="tabon" style="min-width:50px">Blocks</td>
				<td class="tabmin">&nbsp;</td>
				<td id="tab_python" class="taboff">Python</td>
				<!-- <td id="save_js" class="taboff" onclick="savePyToFile()">Save</td> -->
				<td class="tabmin">&nbsp;</td>
				<td id="tab_xml" class="taboff">XML</td>
				<td class="tabmin">&nbsp;</td>
				<td class="tabmin"><input type="text" id="searchBox" style="width: 220px;padding-top: 5px;padding-bottom: 5px;font-size: 16px;vertical-align: text-top;" autocomplete="on" placeholder="Search for blocks (ALT+C)..." title="Type in a name of the block" onkeyup="filter();" accesskey="C"></td>
				<td class="tabmin">&nbsp;</td>
				<td class="tabmin"><button id="searchButton" style="width:100%;padding-top: 7px;padding-bottom: 6px;" title="Put the top result to the workspace" onclick="insertFirstBlock();">Search</td>
				<!--<td id="save_xml" class="taboff" onclick="toXml()">Save</td>-->
				<!-- <td id="save_xml" class="taboff" onclick="saveXML()">Save</td> -->
				<!--<td id="load_xml" class="taboff" onclick="testFunction('Proba')">Load</td>-->
				<td class="tabmax">
				  <button id="trashButton" class="notext" title="...">
					<img src='media/1x1.gif' class="trash icon21">
				  </button>
				  <button id="linkButton" class="notext" title="..." onclick="">
					<img src='media/1x1.gif' class="link icon21">
				  </button>
				  <button id="runButton" class="notext primary" title="..." onclick="goPython();">
					<img src='media/1x1.gif' class="run icon21">
				  </button>
				</td>
			</tr>
		  </table>

		  <table style="height:100%;width:100%;table-layout:fixed;">
			<tr>
				<td id="content_area" rowspan=2 style="height: 100%; width: 70%;">
				</td>
				<!-- <pre id="content_python" class="content"></pre> -->
				<td style="height: 50%; width: 30%;">
				<table style="height: 100%; width: 100%;overflow:scroll;table-layout:fixed;">
					<tr style="height: 8%">
						<td style="font-weight:bold">
							PTC preview:
						</td>
					</tr>
					<tr style="height: 92%">
						<td>
							<pre id="PTC_preview" style="overflow:scroll;height:100%;width:100%;"></pre>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td style="height: 50%; width: 30%;">
				<table style="height: 100%; width: 100%;overflow:scroll;table-layout:fixed;">
					<tr style="height: 8%">
						<td class="preview_title" style="font-weight:bold">
							Expected results:
						</td>
					</tr>
					<tr style="height: 92%">
						<td>
							<!-- <p>Python preview:</p> -->
							<!-- <pre id="Python_preview" style="overflow:scroll;height:100%;width:100%;"></pre> -->
							<pre id="Expected_results_preview" style="overflow:scroll;height:100%;width:100%;"></pre>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>



		<div id="content_blocks" class="content"></div>
		<pre id="content_python" class="content"></pre>
		<textarea id="content_xml" class="content" wrap="off"></textarea>

		  <xml id="toolbox" style="display: none">
	<category name="Search results" colour="100">
		<label text="Search results"></label>
		<block type="controls_if" tooltip="test if"></block>
      <block type="logic_compare" tooltip="test compare"></block>
      <block type="logic_operation" tooltip="test operation"></block>
      <block type="logic_negate" tooltip="test negate"></block>
      <block type="logic_boolean" tooltip="test boolean"></block>
      <block type="logic_null" tooltip="test null"></block>
      <block type="logic_ternary" tooltip="test ternary"></block>
	  <block type="controls_repeat_ext">
        <value name="TIMES">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="controls_whileUntil"></block>
      <block type="controls_for">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
        <value name="BY">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="controls_forEach"></block>
      <block type="controls_flow_statements"></block>
	  <block type="math_number"></block>
      <block type="math_arithmetic">
        <value name="A">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="B">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="math_single">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">9</field>
          </shadow>
        </value>
      </block>
      <block type="math_trig">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">45</field>
          </shadow>
        </value>
      </block>
      <block type="math_constant"></block>
      <block type="math_number_property">
        <value name="NUMBER_TO_CHECK">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="math_round">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">3.1</field>
          </shadow>
        </value>
      </block>
      <block type="math_on_list"></block>
      <block type="math_modulo">
        <value name="DIVIDEND">
          <shadow type="math_number">
            <field name="NUM">64</field>
          </shadow>
        </value>
        <value name="DIVISOR">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="math_constrain">
        <value name="VALUE">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="LOW">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="HIGH">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_int">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_float"></block>
	  <block type="text"></block>
      <block type="text_join"></block>
      <block type="text_append">
        <value name="TEXT">
          <shadow type="text"></shadow>
        </value>
      </block>
      <block type="text_length">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_isEmpty">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT"></field>
          </shadow>
        </value>
      </block>
      <block type="text_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
        <value name="FIND">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_charAt">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_getSubstring">
        <value name="STRING">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_changeCase">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_trim">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_print">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_prompt_ext">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
	  <block type="lists_create_with">
        <mutation items="0"></mutation>
      </block>
      <block type="lists_create_with"></block>
      <block type="lists_repeat">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">5</field>
          </shadow>
        </value>
      </block>
      <block type="lists_length"></block>
      <block type="lists_isEmpty"></block>
      <block type="lists_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getIndex">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_setIndex">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getSublist">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_split">
        <value name="DELIM">
          <shadow type="text">
            <field name="TEXT">,</field>
          </shadow>
        </value>
      </block>
      <block type="lists_sort"></block>
	  <block type="colour_picker"></block>
      <block type="colour_random"></block>
      <block type="colour_rgb">
        <value name="RED">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
        <value name="GREEN">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="BLUE">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="colour_blend">
        <value name="COLOUR1">
          <shadow type="colour_picker">
            <field name="COLOUR">#ff0000</field>
          </shadow>
        </value>
        <value name="COLOUR2">
          <shadow type="colour_picker">
            <field name="COLOUR">#3333ff</field>
          </shadow>
        </value>
        <value name="RATIO">
          <shadow type="math_number">
            <field name="NUM">0.5</field>
          </shadow>
        </value>
      </block>
	  <block type="tedd_tcttemplate_blank"></block>

	<block type="import">
		<field name="import_value">import_time</field>
	</block>

	<block type="import_custom">
		<field name="import_value">custom_import</field>
	</block>


	<block type="import_custom">
      <field name="import_value">Utils.utilities</field>
      <next>
        <block type="klema_15">
          <value name="errorID">
            <block type="math_number">
			  <field name="positive_check">TRUE</field>
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="start_board">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

	<block type="import_custom">
      <field name="import_value">Utils.API_Diagnostic</field>
      <next>
        <block type="check_dtc_status">
          <value name="dtc">
            <block type="text">
              <field name="TEXT"></field>
            </block>
          </value>
          <value name="errorID">
            <block type="math_number">
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="add_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="timeout">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="klema_30">
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_board_status">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>


  <block type="checkerrorehm">
    <field name="positive_check">TRUE</field>
    <value name="error_id">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

  <block type="import_custom">
    <field name="import_value">Utils.API_RTE as RTE</field>
    <next>
      <block type="triggererror">
        <field name="positive_check">TRUE</field>
        <value name="host">
          <block type="text">
            <field name="TEXT"></field>
          </block>
        </value>
        <value name="error_id">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
        <value name="errorID">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
      </block>
    </next>
  </block>

  <block type="flashdataset">
    <field name="positive_check">TRUE</field>
    <value name="DS_name">
      <block type="text">
        <field name="TEXT"></field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

	<!-- <block type="tedd_header"></block>	 -->

	<!-- <block type="tedd_header_int"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="int_value"> -->
        <!-- <block type="math_number"> -->
          <!-- <field name="NUM">0</field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_string"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="string_value"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_list"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="list_value"> -->
        <!-- <block type="lists_create_with"> -->
          <!-- <mutation items="2"></mutation> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<block type="tedd_header_new_item" tooltip="tedd header item">
      <value name="entry_name">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
    </block>

	<!-- <block type="tedd_header"> -->
    <!-- <mutation items="3"></mutation> -->
    <!-- <value name="ADD0"> -->
      <!-- <block type="tedd_header_int"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">PTC_ID</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="int_value"> -->
          <!-- <block type="math_number"> -->
            <!-- <field name="NUM">0</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD1"> -->
      <!-- <block type="tedd_header_string"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_name</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="string_value"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_value</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD2"> -->
      <!-- <block type="tedd_header_list"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">number_list</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="list_value"> -->
          <!-- <block type="lists_create_with"> -->
            <!-- <mutation items="2"></mutation> -->
            <!-- <value name="ADD0"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
            <!-- <value name="ADD1"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
  <!-- </block> -->

	  <block type="tedd_header" tooltip="tedd header template">
		<mutation items="3"></mutation>
		<value name="ADD0">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">PTC_ID</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="math_number">
				<field name="NUM">11223344</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD1">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">string_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="text">
				<field name="TEXT">string_value</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD2">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">list_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="lists_create_with">
				<mutation items="3"></mutation>
				<value name="ADD0">
				  <block type="math_number">
					<field name="NUM">1</field>
				  </block>
				</value>
				<value name="ADD1">
				  <block type="math_number">
					<field name="NUM">2</field>
				  </block>
				</value>
				<value name="ADD2">
				  <block type="math_number">
					<field name="NUM">3</field>
				  </block>
				</value>
			  </block>
			</value>
		  </block>
		</value>
	  </block>



	</category>
    <category name="Logic" colour="%{BKY_LOGIC_HUE}">
	  <label text="Logic"></label>
      <block type="controls_if" tooltip="test if"></block>
      <block type="logic_compare" tooltip="test compare"></block>
      <block type="logic_operation" tooltip="test operation"></block>
      <block type="logic_negate" tooltip="test negate"></block>
      <block type="logic_boolean" tooltip="test boolean"></block>
      <block type="logic_null" tooltip="test null"></block>
      <block type="logic_ternary" tooltip="test ternary"></block>
    </category>
    <category name="Loops" colour="%{BKY_LOOPS_HUE}">
	<label text="Loops"></label>
      <block type="controls_repeat_ext">
        <value name="TIMES">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="controls_whileUntil"></block>
      <block type="controls_for">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
        <value name="BY">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="controls_forEach"></block>
      <block type="controls_flow_statements"></block>
    </category>
    <category name="Math" colour="%{BKY_MATH_HUE}">
	<label text="Math"></label>
      <block type="math_number"></block>
      <block type="math_arithmetic">
        <value name="A">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="B">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="math_single">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">9</field>
          </shadow>
        </value>
      </block>
      <block type="math_trig">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">45</field>
          </shadow>
        </value>
      </block>
      <block type="math_constant"></block>
      <block type="math_number_property">
        <value name="NUMBER_TO_CHECK">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="math_round">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">3.1</field>
          </shadow>
        </value>
      </block>
      <block type="math_on_list"></block>
      <block type="math_modulo">
        <value name="DIVIDEND">
          <shadow type="math_number">
            <field name="NUM">64</field>
          </shadow>
        </value>
        <value name="DIVISOR">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="math_constrain">
        <value name="VALUE">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="LOW">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="HIGH">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_int">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_float"></block>
    </category>
    <category name="Text" colour="%{BKY_TEXTS_HUE}">
	<label text="Text"></label>
      <block type="text"></block>
      <block type="text_join"></block>
      <block type="text_append">
        <value name="TEXT">
          <shadow type="text"></shadow>
        </value>
      </block>
      <block type="text_length">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_isEmpty">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT"></field>
          </shadow>
        </value>
      </block>
      <block type="text_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
        <value name="FIND">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_charAt">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_getSubstring">
        <value name="STRING">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_changeCase">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_trim">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_print">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_prompt_ext">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
    </category>
    <category name="Lists" colour="%{BKY_LISTS_HUE}">
	<label text="Lists"></label>
      <block type="lists_create_with">
        <mutation items="0"></mutation>
      </block>
      <block type="lists_create_with"></block>
      <block type="lists_repeat">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">5</field>
          </shadow>
        </value>
      </block>
      <block type="lists_length"></block>
      <block type="lists_isEmpty"></block>
      <block type="lists_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getIndex">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_setIndex">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getSublist">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_split">
        <value name="DELIM">
          <shadow type="text">
            <field name="TEXT">,</field>
          </shadow>
        </value>
      </block>
      <block type="lists_sort"></block>
    </category>
    <category name="Colour" colour="%{BKY_COLOUR_HUE}">
	<label text="Colour"></label>
      <block type="colour_picker"></block>
      <block type="colour_random"></block>
      <block type="colour_rgb">
        <value name="RED">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
        <value name="GREEN">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="BLUE">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="colour_blend">
        <value name="COLOUR1">
          <shadow type="colour_picker">
            <field name="COLOUR">#ff0000</field>
          </shadow>
        </value>
        <value name="COLOUR2">
          <shadow type="colour_picker">
            <field name="COLOUR">#3333ff</field>
          </shadow>
        </value>
        <value name="RATIO">
          <shadow type="math_number">
            <field name="NUM">0.5</field>
          </shadow>
        </value>
      </block>
    </category>

	<sep></sep>

	<category name="TEDD" colour="%{BKY_LOOPS_HUE}">
	<label text="TEDD"></label>

	<!-- <block type="tedd_custom_code"></block> -->

	<block type="tedd_tcttemplate_blank"></block>

	<block type="import">
		<field name="import_value">import_time</field>
	</block>

	<block type="import_custom">
		<field name="import_value">custom_import</field>
	</block>


	<block type="import_custom">
      <field name="import_value">Utils.utilities</field>
      <next>
        <block type="klema_15">
          <value name="errorID">
            <block type="math_number">
			  <field name="positive_check">TRUE</field>
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="start_board">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

	<block type="import_custom">
      <field name="import_value">Utils.API_Diagnostic</field>
      <next>
        <block type="check_dtc_status">
          <value name="dtc">
            <block type="text">
              <field name="TEXT"></field>
            </block>
          </value>
          <value name="errorID">
            <block type="math_number">
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="add_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="timeout">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="klema_30">
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_board_status">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>


  <block type="checkerrorehm">
    <field name="positive_check">TRUE</field>
    <value name="error_id">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

  <block type="import_custom">
    <field name="import_value">Utils.API_RTE as RTE</field>
    <next>
      <block type="triggererror">
        <field name="positive_check">TRUE</field>
        <value name="host">
          <block type="text">
            <field name="TEXT"></field>
          </block>
        </value>
        <value name="error_id">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
        <value name="errorID">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
      </block>
    </next>
  </block>

  <block type="flashdataset">
    <field name="positive_check">TRUE</field>
    <value name="DS_name">
      <block type="text">
        <field name="TEXT"></field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

	<!-- <block type="tedd_header"></block>	 -->

	<!-- <block type="tedd_header_int"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="int_value"> -->
        <!-- <block type="math_number"> -->
          <!-- <field name="NUM">0</field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_string"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="string_value"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_list"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="list_value"> -->
        <!-- <block type="lists_create_with"> -->
          <!-- <mutation items="2"></mutation> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<block type="tedd_header_new_item" tooltip="tedd header item">
      <value name="entry_name">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
    </block>

	<!-- <block type="tedd_header"> -->
    <!-- <mutation items="3"></mutation> -->
    <!-- <value name="ADD0"> -->
      <!-- <block type="tedd_header_int"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">PTC_ID</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="int_value"> -->
          <!-- <block type="math_number"> -->
            <!-- <field name="NUM">0</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD1"> -->
      <!-- <block type="tedd_header_string"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_name</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="string_value"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_value</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD2"> -->
      <!-- <block type="tedd_header_list"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">number_list</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="list_value"> -->
          <!-- <block type="lists_create_with"> -->
            <!-- <mutation items="2"></mutation> -->
            <!-- <value name="ADD0"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
            <!-- <value name="ADD1"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
  <!-- </block> -->

	  <block type="tedd_header" tooltip="tedd header template">
		<mutation items="3"></mutation>
		<value name="ADD0">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">PTC_ID</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="math_number">
				<field name="NUM">11223344</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD1">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">string_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="text">
				<field name="TEXT">string_value</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD2">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">list_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="lists_create_with">
				<mutation items="3"></mutation>
				<value name="ADD0">
				  <block type="math_number">
					<field name="NUM">1</field>
				  </block>
				</value>
				<value name="ADD1">
				  <block type="math_number">
					<field name="NUM">2</field>
				  </block>
				</value>
				<value name="ADD2">
				  <block type="math_number">
					<field name="NUM">3</field>
				  </block>
				</value>
			  </block>
			</value>
		  </block>
		</value>
	  </block>

	</category>

    <sep></sep>
    <category name="Variables" colour="%{BKY_VARIABLES_HUE}" custom="VARIABLE"><label text="Variables"></label></category>
    <category name="Functions" colour="%{BKY_PROCEDURES_HUE}" custom="PROCEDURE"><label text="Functions"></label></category>
  </xml>

 <xml id="toolboxWorkingCopy" style="display: none">
	<category name="Search results" colour="100">
		<label text="Search results"></label>
		<block type="controls_if" tooltip="test if"></block>
      <block type="logic_compare" tooltip="test compare"></block>
      <block type="logic_operation" tooltip="test operation"></block>
      <block type="logic_negate" tooltip="test negate"></block>
      <block type="logic_boolean" tooltip="test boolean"></block>
      <block type="logic_null" tooltip="test null"></block>
      <block type="logic_ternary" tooltip="test ternary"></block>
	  <block type="controls_repeat_ext">
        <value name="TIMES">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="controls_whileUntil"></block>
      <block type="controls_for">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
        <value name="BY">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="controls_forEach"></block>
      <block type="controls_flow_statements"></block>
	  <block type="math_number"></block>
      <block type="math_arithmetic">
        <value name="A">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="B">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="math_single">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">9</field>
          </shadow>
        </value>
      </block>
      <block type="math_trig">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">45</field>
          </shadow>
        </value>
      </block>
      <block type="math_constant"></block>
      <block type="math_number_property">
        <value name="NUMBER_TO_CHECK">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="math_round">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">3.1</field>
          </shadow>
        </value>
      </block>
      <block type="math_on_list"></block>
      <block type="math_modulo">
        <value name="DIVIDEND">
          <shadow type="math_number">
            <field name="NUM">64</field>
          </shadow>
        </value>
        <value name="DIVISOR">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="math_constrain">
        <value name="VALUE">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="LOW">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="HIGH">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_int">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_float"></block>
	  <block type="text"></block>
      <block type="text_join"></block>
      <block type="text_append">
        <value name="TEXT">
          <shadow type="text"></shadow>
        </value>
      </block>
      <block type="text_length">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_isEmpty">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT"></field>
          </shadow>
        </value>
      </block>
      <block type="text_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
        <value name="FIND">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_charAt">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_getSubstring">
        <value name="STRING">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_changeCase">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_trim">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_print">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_prompt_ext">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
	  <block type="lists_create_with">
        <mutation items="0"></mutation>
      </block>
      <block type="lists_create_with"></block>
      <block type="lists_repeat">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">5</field>
          </shadow>
        </value>
      </block>
      <block type="lists_length"></block>
      <block type="lists_isEmpty"></block>
      <block type="lists_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getIndex">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_setIndex">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getSublist">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_split">
        <value name="DELIM">
          <shadow type="text">
            <field name="TEXT">,</field>
          </shadow>
        </value>
      </block>
      <block type="lists_sort"></block>
	  <block type="colour_picker"></block>
      <block type="colour_random"></block>
      <block type="colour_rgb">
        <value name="RED">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
        <value name="GREEN">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="BLUE">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="colour_blend">
        <value name="COLOUR1">
          <shadow type="colour_picker">
            <field name="COLOUR">#ff0000</field>
          </shadow>
        </value>
        <value name="COLOUR2">
          <shadow type="colour_picker">
            <field name="COLOUR">#3333ff</field>
          </shadow>
        </value>
        <value name="RATIO">
          <shadow type="math_number">
            <field name="NUM">0.5</field>
          </shadow>
        </value>
      </block>
	  <block type="tedd_tcttemplate_blank"></block>

	<block type="import">
		<field name="import_value">import_time</field>
	</block>

	<block type="import_custom">
		<field name="import_value">custom_import</field>
	</block>


	<block type="import_custom">
      <field name="import_value">Utils.utilities</field>
      <next>
        <block type="klema_15">
          <value name="errorID">
            <block type="math_number">
			  <field name="positive_check">TRUE</field>
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="start_board">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

	<block type="import_custom">
      <field name="import_value">Utils.API_Diagnostic</field>
      <next>
        <block type="check_dtc_status">
          <value name="dtc">
            <block type="text">
              <field name="TEXT"></field>
            </block>
          </value>
          <value name="errorID">
            <block type="math_number">
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="add_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="timeout">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="klema_30">
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_board_status">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>


  <block type="checkerrorehm">
    <field name="positive_check">TRUE</field>
    <value name="error_id">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

  <block type="import_custom">
    <field name="import_value">Utils.API_RTE as RTE</field>
    <next>
      <block type="triggererror">
        <field name="positive_check">TRUE</field>
        <value name="host">
          <block type="text">
            <field name="TEXT"></field>
          </block>
        </value>
        <value name="error_id">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
        <value name="errorID">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
      </block>
    </next>
  </block>

  <block type="flashdataset">
    <field name="positive_check">TRUE</field>
    <value name="DS_name">
      <block type="text">
        <field name="TEXT"></field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

	<!-- <block type="tedd_header"></block>	 -->

	<!-- <block type="tedd_header_int"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="int_value"> -->
        <!-- <block type="math_number"> -->
          <!-- <field name="NUM">0</field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_string"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="string_value"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_list"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="list_value"> -->
        <!-- <block type="lists_create_with"> -->
          <!-- <mutation items="2"></mutation> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<block type="tedd_header_new_item" tooltip="tedd header item">
      <value name="entry_name">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
    </block>

	<!-- <block type="tedd_header"> -->
    <!-- <mutation items="3"></mutation> -->
    <!-- <value name="ADD0"> -->
      <!-- <block type="tedd_header_int"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">PTC_ID</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="int_value"> -->
          <!-- <block type="math_number"> -->
            <!-- <field name="NUM">0</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD1"> -->
      <!-- <block type="tedd_header_string"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_name</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="string_value"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_value</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD2"> -->
      <!-- <block type="tedd_header_list"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">number_list</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="list_value"> -->
          <!-- <block type="lists_create_with"> -->
            <!-- <mutation items="2"></mutation> -->
            <!-- <value name="ADD0"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
            <!-- <value name="ADD1"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
  <!-- </block> -->

	  <block type="tedd_header" tooltip="tedd header template">
		<mutation items="3"></mutation>
		<value name="ADD0">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">PTC_ID</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="math_number">
				<field name="NUM">11223344</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD1">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">string_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="text">
				<field name="TEXT">string_value</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD2">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">list_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="lists_create_with">
				<mutation items="3"></mutation>
				<value name="ADD0">
				  <block type="math_number">
					<field name="NUM">1</field>
				  </block>
				</value>
				<value name="ADD1">
				  <block type="math_number">
					<field name="NUM">2</field>
				  </block>
				</value>
				<value name="ADD2">
				  <block type="math_number">
					<field name="NUM">3</field>
				  </block>
				</value>
			  </block>
			</value>
		  </block>
		</value>
	  </block>



	</category>
    <category name="Logic" colour="%{BKY_LOGIC_HUE}">
	  <label text="Logic"></label>
      <block type="controls_if" tooltip="test if"></block>
      <block type="logic_compare" tooltip="test compare"></block>
      <block type="logic_operation" tooltip="test operation"></block>
      <block type="logic_negate" tooltip="test negate"></block>
      <block type="logic_boolean" tooltip="test boolean"></block>
      <block type="logic_null" tooltip="test null"></block>
      <block type="logic_ternary" tooltip="test ternary"></block>
    </category>
    <category name="Loops" colour="%{BKY_LOOPS_HUE}">
	<label text="Loops"></label>
      <block type="controls_repeat_ext">
        <value name="TIMES">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="controls_whileUntil"></block>
      <block type="controls_for">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
        <value name="BY">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="controls_forEach"></block>
      <block type="controls_flow_statements"></block>
    </category>
    <category name="Math" colour="%{BKY_MATH_HUE}">
	<label text="Math"></label>
      <block type="math_number"></block>
      <block type="math_arithmetic">
        <value name="A">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="B">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
      </block>
      <block type="math_single">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">9</field>
          </shadow>
        </value>
      </block>
      <block type="math_trig">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">45</field>
          </shadow>
        </value>
      </block>
      <block type="math_constant"></block>
      <block type="math_number_property">
        <value name="NUMBER_TO_CHECK">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="math_round">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">3.1</field>
          </shadow>
        </value>
      </block>
      <block type="math_on_list"></block>
      <block type="math_modulo">
        <value name="DIVIDEND">
          <shadow type="math_number">
            <field name="NUM">64</field>
          </shadow>
        </value>
        <value name="DIVISOR">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
        </value>
      </block>
      <block type="math_constrain">
        <value name="VALUE">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="LOW">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="HIGH">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_int">
        <value name="FROM">
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="TO">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
      </block>
      <block type="math_random_float"></block>
    </category>
    <category name="Text" colour="%{BKY_TEXTS_HUE}">
	<label text="Text"></label>
      <block type="text"></block>
      <block type="text_join"></block>
      <block type="text_append">
        <value name="TEXT">
          <shadow type="text"></shadow>
        </value>
      </block>
      <block type="text_length">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_isEmpty">
        <value name="VALUE">
          <shadow type="text">
            <field name="TEXT"></field>
          </shadow>
        </value>
      </block>
      <block type="text_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
        <value name="FIND">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_charAt">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_getSubstring">
        <value name="STRING">
          <block type="variables_get">
            <field name="VAR">{textVariable}</field>
          </block>
        </value>
      </block>
      <block type="text_changeCase">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_trim">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_print">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
      <block type="text_prompt_ext">
        <value name="TEXT">
          <shadow type="text">
            <field name="TEXT">abc</field>
          </shadow>
        </value>
      </block>
    </category>
    <category name="Lists" colour="%{BKY_LISTS_HUE}">
	<label text="Lists"></label>
      <block type="lists_create_with">
        <mutation items="0"></mutation>
      </block>
      <block type="lists_create_with"></block>
      <block type="lists_repeat">
        <value name="NUM">
          <shadow type="math_number">
            <field name="NUM">5</field>
          </shadow>
        </value>
      </block>
      <block type="lists_length"></block>
      <block type="lists_isEmpty"></block>
      <block type="lists_indexOf">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getIndex">
        <value name="VALUE">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_setIndex">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_getSublist">
        <value name="LIST">
          <block type="variables_get">
            <field name="VAR">{listVariable}</field>
          </block>
        </value>
      </block>
      <block type="lists_split">
        <value name="DELIM">
          <shadow type="text">
            <field name="TEXT">,</field>
          </shadow>
        </value>
      </block>
      <block type="lists_sort"></block>
    </category>
    <category name="Colour" colour="%{BKY_COLOUR_HUE}">
	<label text="Colour"></label>
      <block type="colour_picker"></block>
      <block type="colour_random"></block>
      <block type="colour_rgb">
        <value name="RED">
          <shadow type="math_number">
            <field name="NUM">100</field>
          </shadow>
        </value>
        <value name="GREEN">
          <shadow type="math_number">
            <field name="NUM">50</field>
          </shadow>
        </value>
        <value name="BLUE">
          <shadow type="math_number">
            <field name="NUM">0</field>
          </shadow>
        </value>
      </block>
      <block type="colour_blend">
        <value name="COLOUR1">
          <shadow type="colour_picker">
            <field name="COLOUR">#ff0000</field>
          </shadow>
        </value>
        <value name="COLOUR2">
          <shadow type="colour_picker">
            <field name="COLOUR">#3333ff</field>
          </shadow>
        </value>
        <value name="RATIO">
          <shadow type="math_number">
            <field name="NUM">0.5</field>
          </shadow>
        </value>
      </block>
    </category>

	<sep></sep>

	<category name="TEDD" colour="%{BKY_LOOPS_HUE}">
	<label text="TEDD"></label>

	<!-- <block type="tedd_custom_code"></block> -->

	<block type="tedd_tcttemplate_blank"></block>

	<block type="import">
		<field name="import_value">import_time</field>
	</block>

	<block type="import_custom">
		<field name="import_value">custom_import</field>
	</block>


	<block type="import_custom">
      <field name="import_value">Utils.utilities</field>
      <next>
        <block type="klema_15">
          <value name="errorID">
            <block type="math_number">
			  <field name="positive_check">TRUE</field>
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="start_board">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

	<block type="import_custom">
      <field name="import_value">Utils.API_Diagnostic</field>
      <next>
        <block type="check_dtc_status">
          <value name="dtc">
            <block type="text">
              <field name="TEXT"></field>
            </block>
          </value>
          <value name="errorID">
            <block type="math_number">
              <field name="NUM">0</field>
            </block>
          </value>
        </block>
      </next>
    </block>

    <block type="add_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_condition">
      <value name="host">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="condition">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
      <value name="timeout">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="klema_30">
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>

    <block type="check_board_status">
      <value name="state">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
      <value name="errorID">
        <block type="math_number">
          <field name="NUM">0</field>
        </block>
      </value>
    </block>


  <block type="checkerrorehm">
    <field name="positive_check">TRUE</field>
    <value name="error_id">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

  <block type="import_custom">
    <field name="import_value">Utils.API_RTE as RTE</field>
    <next>
      <block type="triggererror">
        <field name="positive_check">TRUE</field>
        <value name="host">
          <block type="text">
            <field name="TEXT"></field>
          </block>
        </value>
        <value name="error_id">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
        <value name="errorID">
          <block type="math_number">
            <field name="NUM">0</field>
          </block>
        </value>
      </block>
    </next>
  </block>

  <block type="flashdataset">
    <field name="positive_check">TRUE</field>
    <value name="DS_name">
      <block type="text">
        <field name="TEXT"></field>
      </block>
    </value>
    <value name="errorID">
      <block type="math_number">
        <field name="NUM">0</field>
      </block>
    </value>
  </block>

	<!-- <block type="tedd_header"></block>	 -->

	<!-- <block type="tedd_header_int"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="int_value"> -->
        <!-- <block type="math_number"> -->
          <!-- <field name="NUM">0</field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_string"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="string_value"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<!-- <block type="tedd_header_list"> -->
      <!-- <value name="entry_name"> -->
        <!-- <block type="text"> -->
          <!-- <field name="TEXT"></field> -->
        <!-- </block> -->
      <!-- </value> -->
      <!-- <value name="list_value"> -->
        <!-- <block type="lists_create_with"> -->
          <!-- <mutation items="2"></mutation> -->
        <!-- </block> -->
      <!-- </value> -->
    <!-- </block> -->

	<block type="tedd_header_new_item" tooltip="tedd header item">
      <value name="entry_name">
        <block type="text">
          <field name="TEXT"></field>
        </block>
      </value>
    </block>

	<!-- <block type="tedd_header"> -->
    <!-- <mutation items="3"></mutation> -->
    <!-- <value name="ADD0"> -->
      <!-- <block type="tedd_header_int"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">PTC_ID</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="int_value"> -->
          <!-- <block type="math_number"> -->
            <!-- <field name="NUM">0</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD1"> -->
      <!-- <block type="tedd_header_string"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_name</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="string_value"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">string_value</field> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
    <!-- <value name="ADD2"> -->
      <!-- <block type="tedd_header_list"> -->
        <!-- <value name="entry_name"> -->
          <!-- <block type="text"> -->
            <!-- <field name="TEXT">number_list</field> -->
          <!-- </block> -->
        <!-- </value> -->
        <!-- <value name="list_value"> -->
          <!-- <block type="lists_create_with"> -->
            <!-- <mutation items="2"></mutation> -->
            <!-- <value name="ADD0"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
            <!-- <value name="ADD1"> -->
              <!-- <block type="math_number"> -->
                <!-- <field name="NUM">0</field> -->
              <!-- </block> -->
            <!-- </value> -->
          <!-- </block> -->
        <!-- </value> -->
      <!-- </block> -->
    <!-- </value> -->
  <!-- </block> -->

	  <block type="tedd_header" tooltip="tedd header template">
		<mutation items="3"></mutation>
		<value name="ADD0">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">PTC_ID</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="math_number">
				<field name="NUM">11223344</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD1">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">string_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="text">
				<field name="TEXT">string_value</field>
			  </block>
			</value>
		  </block>
		</value>
		<value name="ADD2">
		  <block type="tedd_header_new_item">
			<value name="entry_name">
			  <block type="text">
				<field name="TEXT">list_name</field>
			  </block>
			</value>
			<value name="entry_value">
			  <block type="lists_create_with">
				<mutation items="3"></mutation>
				<value name="ADD0">
				  <block type="math_number">
					<field name="NUM">1</field>
				  </block>
				</value>
				<value name="ADD1">
				  <block type="math_number">
					<field name="NUM">2</field>
				  </block>
				</value>
				<value name="ADD2">
				  <block type="math_number">
					<field name="NUM">3</field>
				  </block>
				</value>
			  </block>
			</value>
		  </block>
		</value>
	  </block>

	</category>

    <sep></sep>
    <category name="Variables" colour="%{BKY_VARIABLES_HUE}" custom="VARIABLE"><label text="Variables"></label></category>
    <category name="Functions" colour="%{BKY_PROCEDURES_HUE}" custom="PROCEDURE"><label text="Functions"></label></category>
  </xml>

    </body>
</html>
